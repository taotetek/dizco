/*  =========================================================================
    zetcd - class description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of normalz.                                      
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef ZETCD_H_INCLUDED
#define ZETCD_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

//  Include the library file with typdefs, public includes and public constants
#include "zetcd_library.h"

//  @interface
//  Create a new zetcd
ZETCD_EXPORT zetcd_t *
    zetcd_new (char *endpoints);

//  Destroy the zetcd
ZETCD_EXPORT void
    zetcd_destroy (zetcd_t **self_p);

//  Self test of this class
ZETCD_EXPORT void
    zetcd_test (bool verbose);

//  @end

#ifdef __cplusplus
}
#endif

#endif
