/*  =========================================================================
    zetcd - class description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of normalz.                                      
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    zetcd - 
@discuss
@end
*/

#include "zetcd_classes.h"

//  Structure of our class

struct _zetcd_t {
    CURL *curl_handle;
    CURLcode curl_resp_code;
    json_object *json;
    zlist_t *endpoints;
    char url_buf[1024];
};


//  --------------------------------------------------------------------------
//  Create a new zetcd

zetcd_t *
zetcd_new (char *endpoints)
{
    zetcd_t *self = (zetcd_t *) zmalloc (sizeof (zetcd_t));
    assert (self);

    curl_global_init (CURL_GLOBAL_ALL);
    self->curl_handle = curl_easy_init ();
    assert (self->curl_handle);

    char buf[512];
    assert (buf);

    self->endpoints = zlist_new ();
    assert (self->endpoints);

    while (*endpoints) {
        char *delimiter = strchr(endpoints, ',');
        if (!delimiter) {
            delimiter = endpoints + strlen (endpoints);
        }
        memcpy (buf, endpoints, delimiter - endpoints);
        buf[delimiter - endpoints] = 0;
        char *current = strdup(buf);
        zlist_append (self->endpoints, current);
        if (*delimiter == 0) {
            break;
        }
        endpoints = delimiter + 1;
    }

    return self;
}

char*
zetcd_get_url (zetcd_t *self, const char *group)
{
    char *etcd_url = (char *) zlist_first (self->endpoints);
    strcpy (self->url_buf, etcd_url);
    strcat (self->url_buf, "/v2/keys/");
    strcat (self->url_buf, group);
    return self->url_buf;
}

//  --------------------------------------------------------------------------
//  Register an endpoint

int
zetcd_register_endpoint (zetcd_t *self, const char *group, const char *id, const char *endpoint) 
{
    return 0;
}

//  --------------------------------------------------------------------------
//  List endpoints

zlist_t *
zetcd_list_endpoints (const char *group)
{
    zlist_t *members = zlist_new ();
    return members;
}

//  --------------------------------------------------------------------------
//  Destroy the zetcd

void
zetcd_destroy (zetcd_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        zetcd_t *self = *self_p;
        curl_easy_cleanup (self->curl_handle);
        curl_global_cleanup ();
        zlist_destroy (&self->endpoints);
        free (self);
        *self_p = NULL;
    }
}

//  --------------------------------------------------------------------------
//  Self test of this class

void
zetcd_test (bool verbose)
{
    printf (" * zetcd: ");

    //  @selftest
    //  Simple create/destroy test
    zetcd_t *self = zetcd_new ("http://127.0.0.1:2379");
    assert (self);

    char *endpoint = (char *) zlist_first (self->endpoints);
    assert (streq (endpoint, "http://127.0.0.1:2379"));

    char *next_url = zetcd_get_url (self, "test");
    assert (streq (next_url, "http://127.0.0.1:2379/v2/keys/test"));

    zetcd_destroy (&self);
    //  @end
    printf ("OK\n");
}
